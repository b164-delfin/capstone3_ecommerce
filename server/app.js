//put all dependencies
const dotenv = require('dotenv');
const express = require('express');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const connectDB = require('./database/dbConn');
const productRoutes = require('./routes/productRoutes');


//initialize 
const app = express();

//Configure ENV File & Require Connection File
dotenv.config({path : './.env'});
require('./database/dbConn');
const port = process.env.PORT;

connectDB();

// Require Model
const Users = require('./models/userSchema');
const Message = require('./models/msgSchema');
const authenticate = require('./middleware/auth');
const Subscriber = require('./models/subSchema');
const Product = require('./models/productSchema');

// These Method is Used to Get Data from FrontEnd
app.use(express.json());
app.use(express.urlencoded({extended : true}));
app.use(cookieParser());

app.get('/', (req, res) => {
	res.send("Hello World");
})

// Registration
app.post('/signup', async (req, res) => {
	try {
		// Get body or Data
		const fname = req.body.fname;
		const lname = req.body.lname;
		const email = req.body.email;
		const password = req.body.password;

		const createUser = new Users({
			fname : fname,
			lname : lname,
			email : email,
			password : password
		});

		// Save Method is Used to Create User or Insert User 
		// But Before Saving or Inserting, Password will Hash
		//Because of Hashing. After Hash, it will save to DB
		const created = await createUser.save();
		console.log(created);
		res.status(200).send("Registered");


	} catch (error) {
		res.status(400).send(error);
	}
})

// Login User 
app.post('/login', async (req, res) =>{
	try {
		const email = req.body.email;
		const password = req.body.password;

		// Find User if Exist
		const user = await Users.findOne({email : email});
		if(user){
			const isMatch = await bcryptjs.compare(password, user.password);

			if(isMatch){
				const token = await user.generateToken();
				res.cookie("jwt", token, {
					expires : new Date(Date.now() + 86400000),
					httpOnly : true
				})
				res.status(200).send("Logged In");
			} else {
				res.status(400).send("Invalid Credentials");
			}

		} else {
			res.status(400).send("Invalid Credentials");
		}


	} catch (error) {
		res.status(400).send(error);
	}
})



// Message
app.post('/message', async (req, res) => {
	try {
		// Get body or Data
		const name = req.body.name;
		const email = req.body.email;
		const message = req.body.message;

		const sendMsg = new Message({
			name : name,
			email : email,
			message : message
		});

		// Save Method is Used to Create User or Insert User 
		// But Before Saving or Inserting, Password will Hash
		//Because of Hashing. After Hash, it will save to DB
		const created = await sendMsg.save();
		console.log(created);
		res.status(200).send("Message Sent");


	} catch (error) {
		res.status(400).send(error);
	}
})

//Subscribe
app.post('/subscribe', async (req, res) => {
	try {
		// Get body or Data
		const email = req.body.email;

		const setSubs = new Subscriber({
			email : email
		});

		// Save Method is Used to Create User or Insert User 
		// But Before Saving or Inserting, Password will Hash
		//Because of Hashing. After Hash, it will save to DB
		const created = await setSubs.save();
		console.log(created);
		res.status(200).send("Subscribed");


	} catch (error) {
		res.status(400).send(error);
	}
})



//Logout Page
app.get('/logout', (req, res) => {
	res.clearCookie("jwt", {path : '/'})
	res.status(200).send("User Logged Out")
}) 

//Authentication

app.get('/auth', authenticate, (req, res) => {

})


app.use("/products", productRoutes)

// Run Server

app.listen(port, () => {
	console.log(`Server is running at ${process.env.PORT}`)
}) 