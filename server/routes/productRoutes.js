const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../middleware/auth');



router.post('/addProduct',  (req, res) => {
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
})

router.get('/allProducts', (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})




module.exports = router;